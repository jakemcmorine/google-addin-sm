const { google } = require('googleapis');
const axios = require('axios');
const { v4: uuidv4 } = require('uuid');
const { pool } = require('../models/model');
const authService = require('../services/AuthServices');
const constants = require("../config/constants");
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const redirectURL = process.env.AUTH_REDIRECT;
const optOutEmail = process.env.OPT_OUT_EMAIL;

const { checkUserWhenDelete } = require('./MeetingController');

const rmController = require('../controllers/RecurringMeetingController');

var sqsService = require('../services/sqsService');


const oauth2Client = new google.auth.OAuth2(
    clientId,
    clientSecret,
    redirectURL
);

module.exports.gAuthUser = async (req, res) => {
    try {
        const { query } = req;
        if (query.code) {
            let { tokens } = await oauth2Client.getToken(query.code);
            tokens.email = await authService.verifyToken(tokens.id_token);
            tokens.auth_code = query.code;
            if (!tokens.email) {

            }
            if (tokens.email) {

                checkWebhookChannel(tokens).then(result => {
                    if (result['id']) {
                        tokens.webhookId = result.id;
                        tokens.expirationTime = new Date(result.expirationTime).toISOString()
                        tokens.resourceId = result.resourceId;
                    }
                    authService.saveOrUpdateUserToken(tokens, result.action);
                }).catch(error => {
                    console.log("Error occurred while creating webhook channel :", error);
                    throw error;
                });

            } else {
                throw { eroor: 401, message: 'No email found' }
            }
            res.send('Thanks...... <script>setTimeout(function() { top.window.close() }, 2000);</script>')
        }
    } catch (error) {
        console.log(error);
        res.send('Error!!');

    }
}


/**
 * Following function will check if the webhook channel for thi user exists or not.
 * If not create a webhook channel.
 * @param {*} tokens 
 */
async function checkWebhookChannel(tokens, res) {
    //Check if the webhook channel already exists for this user
    let action = "";
    return new Promise(async (resolve, reject) => {

        const existingWebhook = await pool.query('SELECT webhookId, expirationTime FROM `google_user_tokens` WHERE email = ?', tokens.email);

        if (existingWebhook.length > 0) {

            /**
             * If webhook is expired, then create a webhook channel.
             * else : updateTokens
             */
            if (existingWebhook[0].webhookId && new Date(existingWebhook[0].expirationTime).getTime() < new Date().getTime()) {
                console.log("---Updating/Creating Channel ---");
                action = "update";
                createWebhookChannel(resolve, reject, tokens, action);

            } else {
                console.log("---Updating Tokens ---");
                resolve({ action: 'updateTokens' })
            }
        } else {
            //create for new entry
            console.log("--- Creating Channel ---");
            action = "create"
            createWebhookChannel(resolve, reject, tokens, action);
        }

    })
}


/**
 * Following method is to crete a webhook channel 
 */
function createWebhookChannel(resolve, reject, tokens, action) {
    console.log("action", action);
    let currentDate = new Date();
    currentDate.setMinutes(currentDate.getMinutes() + 5);
    //Create the webhook channel
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${tokens.access_token}`
    }
    //channel for maximum 1 month.
    let channelBody = {
        id: uuidv4(),
        type: 'web_hook',
        address: process.env.NOTIFICATION_URL,
        params: {
            "ttl": 7776000
        }
    }

    let url = constants.GOOGLE_CALENDAR_URL + `/events/watch`;

    axios.post(url, channelBody, { headers }).then(response => {
        //console.log("Response Received Response Received",response);
        resolve({
            id: response.data.id, expirationTime: parseInt(response.data.expiration),
            resourceId: response.data.resourceId, action
        });
    }).catch(error => {
        console.log(error);
        reject(error);
    });
}

/**
 * Following is the registered notification url for event notifications.
 * 1. This will be called on creation of webhook channel, and
 * 2. when any change is made to the calendar of those users whose channel is subscribed.
 */
module.exports.webhookNotification = async (req, res) => {
    try {
        let resourceState = req.headers['x-goog-resource-state'];
        let webhookId = req.headers['x-goog-channel-id'];
        let resourceId = req.headers['x-goog-resource-id'];

        let userTokenDetails = {}; //This object will consist of user token details which will be from the db.
        let headers = {}; // This is used as request headers for calling google api. Will consis of Authorization as property
        let url = ""; //Based on type of channel activity ; Either created or event received. Url value will be changed to call google api.

        /**
         * Following condition checks if resourceTYpe is :
         * sync : This indicates that the channle is created. Perform actions before receiving latest event updates
         * exists : This indicates that the channel already exists and we can call google api to get event data
         */
        if (resourceState === constants.SYNC) {
            console.log("----Resource state : " + resourceState + "Getting All Events. Created Webhook Channel ---" + webhookId);
            let userTokenDetails = await authService.getUserTokenDetails(webhookId);
            if (userTokenDetails != undefined) {
                headers = {
                    'Authorization': `Bearer ${userTokenDetails['access_token']}`
                }
                url = constants.GOOGLE_CALENDAR_URL + '/events';
                // console.log("url : ", url, " headers : ", headers, "  user: ", userTokenDetails['email']);
                // Make an API call and get the latest sync token
                axios.get(url, { headers }).then(response => {

                    if (response && response !== null) {

                        let nextSyncToken = response.data['nextSyncToken'];
                        if (nextSyncToken) {
                            console.log("Next Sync Token is : ", nextSyncToken);
                            //Update the table filed for this user with nextSyncToken
                            authService.saveOrUpdateUserToken({ nextSyncToken, email: userTokenDetails['email'] }, 'updateSyncToken');
                        } else if (response.data['nextPageToken']) {
                            getNextSyncToken({
                                nextPageToken: response.data['nextPageToken'],
                                access_token: userTokenDetails['access_token'],
                                email: userTokenDetails['email']
                            })
                        }
                    }
                }).catch(error => {
                    console.log('Error occurred while geting events.. please console');
                });
            }
        } else {
            console.log("----Resource state : " + resourceState + "----Getting updated event. Notification Received ---");
            userTokenDetails = await authService.getUserTokenDetails(webhookId);

            if (userTokenDetails !== undefined) {
                headers = {
                    'Authorization': `Bearer ${userTokenDetails['access_token']}`
                }
                url = constants.GET_LATEST_EVENT + userTokenDetails['syncToken'];
                axios.get(url, { headers })
                    .then(response => { processChannelData(response, userTokenDetails); })
                    .catch(async error => {

                        console.log(`Error status : ${error.response.status} ; Error Message, ${error.response.statusText}`);

                        if (error.response.status === 401 && error.response.statusText === 'Unauthorized') {
                            await refreshGetEvents(userTokenDetails['syncToken'], userTokenDetails);
                        }
                        console.log('Error occurred while geting events.. or subsscription has been ended.. please console');
                    })
            }

            //Make an API call using the sync token saved and get the latest event and then save the newSyncToken

        }

        res.sendStatus(200);
    }
    catch (error) {
        console.log("-----Error Occurred ------ : ", error);
    }

}

const getNextSyncToken = async tokens => {
    try {
        headers = {
            'Authorization': `Bearer ${tokens['access_token']}`
        }
        let url = constants.GET_PAGINATING_EVENT;
        url = url + tokens.nextPageToken;
        let response = await axios.get(url, { headers });
        if (response.data['nextSyncToken']) {
            await authService.saveOrUpdateUserToken({ nextSyncToken: response.data['nextSyncToken'], email: tokens['email'] }, 'updateSyncToken');
            return true;
        } else if (response.data['nextPageToken']) {
            tokens.nextPageToken = response.data['nextPageToken'];
            getNextSyncToken(tokens);
        } else {
            return true;
        }
    } catch (error) {
        console.error(error);
        throw error;
    }
}

/**
 * Following function is called upon successfully get latest event from the webhook channel.
 * Writing it in seperate function as the same piece of code is used by refreshGetEvents() method.
 * @param {*} response : Object
 * @param {*} userTokenDetails : Object
 */
async function processChannelData(response, userTokenDetails) {

    if (response && response !== null && response.data.kind == "calendar#events") {

        let nextSyncToken = response.data['nextSyncToken']; //This variable is used to save nextSyncToken to be saved in db for further use to get latest events
        let isRecurring = false; //This variable determines whether the meeting is rm or not.

        if (nextSyncToken) {

            authService.saveOrUpdateUserToken({ nextSyncToken, email: userTokenDetails['email'] }, 'updateSyncToken');

            for (let event of response.data.items) {
                // console.log("eventss", response.data);
                /** checking the opt-out functionality, if the opt-out email is exist or not */
                let optOutVal = [];
                if (event.attendees) {
                    let attendeesList = event.attendees;
                    optOutVal = attendeesList.filter(function (listItem) {
                        return listItem.email == optOutEmail;
                    });
                }
                /** If item received has recurrence field . Consider it as am RM else Normal Meeting. */
                isRecurring = (event.recurrence || event.recurringEventId) ?
                    constants.RECURRING_MEETING : (event.status == "cancelled") ? constants.CANCELLED : constants.NORMAL_MEETING

                // console.log("eventssss", event);

                switch (isRecurring) {
                    case constants.RECURRING_MEETING:
                        /** Checking the event owner, event has attendees and the opt-out email  */
                        if ((optOutVal.length == 0 && event.attendees && event.organizer.email === userTokenDetails['email']) || (event.recurringEventId && event.status === 'cancelled')) {
                        //TO DO : Handle Recurring Meeting Creation Flow.
                        rmController.saveRecurringMeetingData(event, userTokenDetails, response.data);
                        }
                        break;
                    case constants.NORMAL_MEETING:
                        console.log("---------- Saving Normal Meeting Data ----------");

                        /** Checking the event owner, event has attendees and the opt-out email  */
                        if (optOutVal.length == 0 && event.attendees && event.organizer.email === userTokenDetails['email']) {

                            // createMeeting(response.data)
                            let meetData = { items: [event], timeZone: response.data.timeZone, summary: response.data.summary };

                            /********* Send data to sqs ****************************************************/
                            var sqs_type = "normal_meeting";
                            var messageGroupID = userTokenDetails['email'];
                            var sqs_data = meetData
                            sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
                            /******* End of Send data to sqs **********************************************/


                            //TO DO : Handle Normal Meeting Creation Flow
                        }
                        break;
                    case constants.CANCELLED:
                        // TO DO : CANCEL NORMAL MMETING
                        console.log("---------- Meeting Cancelled ----------");
                        console.log("evenet event", event);

                        rmController.cancelRecurringMeeting(event, userTokenDetails);
                        let meetingId = event.id;
                        let userEmailData = await checkUserWhenDelete(meetingId);
                        if (userEmailData) {
                            if (userEmailData == userTokenDetails['email']) {

                                /******************** Send data to sqs ***********************************/
                                var sqs_type = "cancelled_meeting";
                                var messageGroupID = userTokenDetails['email'];
                                var sqs_data = meetingId;
                                sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
                                /******* End of Send data to sqs ***************************************/
                            }
                        }

                        break;
                }
            }
        }
    }
}

/**
 * This function is to get events - if access token expire then call this function again 
 * to get the new refresh token and get event data
 * @param {*} syncToken 
 * @param {*} userTokenDetails 
 */
const refreshGetEvents = async (syncToken, userTokenDetails) => {
    try {
        let newAccessToken = await refreshAccessToken(userTokenDetails['email']);
        headers = {
            'Authorization': `Bearer ${newAccessToken}`
        }
        userTokenDetails['access_token'] = newAccessToken;
        url = constants.GET_LATEST_EVENT + syncToken;
        axios.get(url, { headers }).then(response => {
            console.log('--- Event Data Response if access token expires ---');
            processChannelData(response, userTokenDetails);
        }).catch(error => {
            console.log('Error occurred while geting events refresh access token or the subscription ended. Please console for more logs');
        });

    } catch (error) {
        throw error;
    }
}



const refreshAccessToken = async email => {
    try {
        let userData = await pool.query('SELECT refresh_token FROM google_user_tokens  WHERE email = ?', [email]);

        const { refresh_token } = userData[0];
        //when user uninstall and remove the thirdparty access - then the refresh token is invalid it will go to catch block then ask for activation screen on the front end by apps script. after activate it will create a new refresh and access token. 
        const res = await axios.post('https://oauth2.googleapis.com/token', {
            client_id: clientId,
            client_secret: clientSecret,
            grant_type: 'refresh_token',
            refresh_token: refresh_token
        });
        let access_token = res.data.access_token;
        await authService.saveOrUpdateUserToken({ email, access_token }, "updateAccessTokensWithEmail");
        return access_token;
    } catch (error) {
        console.error("Token validation error");
        throw error;
    }
}

module.exports.validateRefreshToken = async (req, res) => {
    try {
        let emailId = req.body.email;

        let checkApi = await axios.post(process.env.VALIDATE_URL, {
            "Emails": [emailId]
        });
        if (checkApi.data != null) {
            let userInDB = await authService.checkAppScritAccess(emailId);
            if (userInDB.length > 0) {
                let token = await refreshAccessToken(emailId);
                return res
                    .status(200)
                    .json({ success: true, access: true });
            } else {
                return res
                    .status(200)
                    .json({ success: true, message: "Account Not Found", access: false });
            }
        } else {
            return res
                .status(200)
                .json({ success: true, message: "invalidAccount", access: false });
        }
    } catch (error) {
        return res
            .status(200)
            .json({ success: true, message: "Auth Needed", access: false });
    }
}


module.exports.refreshAccessTokenCall = async (req, res) => {
    await refreshAccessToken(req.body.email);
    res.sendStatus(200);
}