const meetingRepository = require('../services/MeetingServices');
const Jimp = require('jimp');
const path = require('path');
var moment = require('moment-timezone');
const { pool } = require('../models/model');

const recurrenceController = require('../controllers/RecurringMeetingController');
// const {  } = require('../services/MeetingServices');

/**
 * The following method gives meeting count of a particular user based on user_id.
 * THis gives the number of meetings attended and number of tress planted based on countable field ( 1 or 0)
 */
module.exports.meetingCountOfUser = async (req, res) => {

  console.log(`---Request Received with userId : ${req.params.userId}`)
  let userId = req.params.userId;
  let initialTreeCount = (req.body.treeNumber !== null && req.body.treeNumber !== undefined) ? req.body.treeNumber : 0;

  //Check whether the user is an IP or not.
  let isUserIp = false;
  if (userId !== undefined) {
    isUserIp = await meetingRepository.checkUserIp(userId);
  }
  let meetingsCount;
  let tressPlanted = 0;

  if (isUserIp) {
    console.log("entered")
    if (userId !== null && userId !== "" && userId !== undefined) {
      console.log(`------------------Calling meeting count for an IP : ${userId}-----------------------`);

      let meetings = await meetingRepository.meetingCount("org_id", userId);
      meetingsCount = meetings[0].count;

      let meetingInvite = await meetingRepository.meetingInviteCount("org_id", userId);
      console.log(meetingInvite);
      if (meetings == undefined || meetings == null)
        meetingsCount = 0;

      if (meetingInvite !== undefined || meetingInvite[0] !== undefined ||
        meetingInvite[0]['dataValues'] !== undefined || meetingInvite[0]['dataValues'] !== null)
        tressPlanted = meetingInvite[0]['count'] + initialTreeCount;

    } else {
      console.log("Invalid user id : ", userId);
      meetingsCount = 0;
      tressPlanted = initialTreeCount;
    }
    console.log(meetingsCount);
    console.log(tressPlanted);

  } else {

    console.log("---- User is not an IP and id is : ", userId);
    if (userId !== null && userId !== "" && userId !== undefined) {
      try {
        const meetings = await meetingRepository.meetingCount("user_id", userId);
        meetingsCount = meetings[0]['count'];

        const meetingInvite = await meetingRepository.meetingInviteCount("user_id", userId);
        tressPlanted = meetingInvite[0]['count'] ? meetingInvite[0]['count'] : 0;

        console.log("-------------meetings = ", meetings, "---------------------meetingInvite ---------", meetingInvite[0]['count']);

        // res.json({ meetingCount: mettings, meetingInviteCount: meetingInvite[0]['dataValues'].meeting_invites_Count })

      } catch (error) {
        console.log("Error Occurred while getting meetings count : ", error);
        meetingsCount = 0;
        tressPlanted = 0;
      }

    } else {
      console.log("Invalid user id : ", userId);
      meetingsCount = 0;
      tressPlanted = 0;
    }

  }

  Jimp.read(path.join(__dirname, "../assets/counts.jpg")).then(image => {
    loadedImage = image
    return Jimp.loadFont(Jimp.FONT_SANS_32_BLACK);
  }).then(font => {

    return loadedImage
      .print(font, 0, 220, {
        text: meetingsCount !== null ? `${meetingsCount}` : `0`,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_TOP
      }, 350, 80)
      .print(font, 0, 50, {
        text: tressPlanted !== null ? `${tressPlanted}` : `0`,
        alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
        alignmentY: Jimp.VERTICAL_ALIGN_TOP
      }, 350, 80);

  }).then(image => {
    image.write(path.join(__dirname, "../assets/outputMa.jpg"), res.sendFile(path.join(__dirname, "../assets/outputMa.jpg")));
  });

};

module.exports.checkUserWhenDelete = async (meetId) => {
try {
  let userEmail = await pool.query(`select u.email from meetings m left join users u on m.user_id = u.id where m.meeting_id = ? or rm_id = ?`, [meetId, meetId]);
  if(userEmail.length > 0) {
    return userEmail[0].email;
  } else {
    return false;
  }
} catch(err) {
  console.log("error data", err);
}
};


module.exports.cronCheckRecurringMeetings = async (req, res) => {
  try {
   let data = req.body;
   let eventData  = data.eventDt;
   let userData   = data.userTokenDetails;
   let resData    = data.responseData;
   let resltData  = await recurrenceController.saveRecurringMeetingData(eventData, userData, resData);
   return res
      .status(200)
      .json({ success: true, message: "meetingsAdded"});

  } catch(err) {
    console.log("error data", err);
  }
  };

// module.exports.createMeeting = async (data) => {
//   try {
//     let items = data.items[0];
//     const userDetails = await getUserDetailsWithEmail(data.summary);

//     if (userDetails) {

//       let meetings = await getMeetingWithId(items.id);
//       let attendees = items.attendees;
//       const firstDate = new Date().setHours(12, 0, 0, 0);
//       const secondDate = new Date(userDetails.trialEnd);
//       let user_status = userDetails.user_status;

//       if (secondDate > firstDate) {
//         user_status = 'active'
//       }

//       let meetData = {
//         calendar_type: "gmail",
//         google_meeting_id: items.iCalUID,
//         meeting_id: items.id,
//         subject: items.summary,
//         location: items.location,
//         type: items.type, //?
//         start_date: items.start.dateTime || items.start.date,
//         end_date: items.end.dateTime || items.end.date,
//         TimeZoneOffset: moment.tz(data.timeZone).format("Z"),
//         TimeZoneName: data.timeZone,
//         cancellation_date: null,
//         user_id: userDetails.id,
//         all_Day: items.start.date ? 1 : 0,
//         org_id: userDetails.orginasation_id,
//         initiativeId: 182,
//         countable: 1
//       }

//       userDetails.startTime = meetData.start_date;
//       userDetails.TimeZoneOffset = meetData.TimeZoneOffset;
//       userDetails.TimeZoneName = meetData.TimeZoneName;

//       if (user_status == 'inactive' || user_status == 'force_inactive') {
//         meetData.countable = 0
//       }
//       let meetingSave;
//       if (meetings.length > 0) {
//         meetingSave = await updateMeeting(meetData, meetings);
//       } else {
//         meetingSave = await createMeeting(meetData);
//       }
//       let meetingId = meetingSave.insertId || [meetings[0].id];
//       if (meetingId) {
//         let invites = createInvites(attendees, meetingId);
//         let = await removeOldInviteeAndAddNew(invites, meetingId);
//       }
//     }
//   } catch (error) {
//     console.error(error);
//   }
// }

// const getUserDetailsWithEmail = async email => {
//   let qu = `
//     SELECT
//       users.id,
//       u1.status AS user_status,
//       u1.trialEnd,
//       settings.id AS setting_id,
//       users.orginasation_id,
//       settings.emailSend,
//       settings.hour,
//       settings.minute,
//       settings.exclude_meeting,
//       settings.domain_url
//     FROM
//       users
//     LEFT JOIN settings ON settings.user_id = users.orginasation_id
//     LEFT JOIN users u1 ON
//       users.orginasation_id = u1.id
//     WHERE
//     users.email = ?
//   `
//   const user = await pool.query(qu, [email]);
//   return user[0];
// }

// const getMeetingWithId = async id => {
//   return await pool.query(`SELECT * FROM meetings WHERE meeting_id = ?`, id);
// }


// const createInvites = (obj, meeting_id) => {
//   let inviteList = [];
//   obj.filter(el => {
//     if (!el.organizer && el.responseStatus != "declined") {
//       inviteList.push([
//         meeting_id,
//         el.email,
//         el.email
//       ]);
//     }
//   });
//   console.log("innnnnnnnnnnnn", inviteList);
//   return inviteList;
// }

// const removeOldInviteeAndAddNew = async (invites, meetingId) => {
//   await removeInvitees(meetingId);
//   if (invites.length > 0) {
//     return await insertInvitees(invites);
//   } else {
//     return 0;
//   }
// }

// const insertInvitees = async invites => {
//   let qu = `INSERT INTO meeting_invites(meeting_id, email, first_name) VALUES ?`;
//   return await pool.query(qu, [invites]);
// }

// const removeInvitees = async meeting_id => {
//   return await pool.query(`DELETE FROM meeting_invites WHERE meeting_id = ?`, meeting_id);
// }

// const createMeeting = async meetingData => {
//   try {
//     let qu = `INSERT INTO meetings( calendar_type, meeting_id, subject, location, start_date, end_date, TimeZoneOffset, TimeZoneName, user_id, all_Day, google_meeting_id, org_id, initiativeId, countable ) VALUES(?)`
//     let insVal = [
//       meetingData.calendar_type,
//       meetingData.meeting_id,
//       meetingData.subject,
//       meetingData.location,
//       meetingData.start_date,
//       meetingData.end_date,
//       meetingData.TimeZoneOffset,
//       meetingData.TimeZoneName,
//       meetingData.user_id,
//       meetingData.all_Day,
//       meetingData.google_meeting_id,
//       meetingData.org_id,
//       meetingData.initiativeId,
//       meetingData.countable
//     ]
//     return await pool.query(qu, [insVal]);
//   } catch (error) {
//     throw error;
//   }
// }

// const updateMeeting = async (meetingData, meetings) => {
//   try {
//     let qu = `UPDATE meetings SET subject = ?, location = ?, start_date = ?, end_date = ?, TimeZoneOffset = ?, TimeZoneName = ?, all_Day = ?, initiativeId = ? WHERE google_meeting_id = ?`;
//     let dataForUpdate = [
//       meetingData.subject,
//       meetingData.location,
//       meetingData.start_date,
//       meetingData.end_date,
//       meetingData.TimeZoneOffset,
//       meetingData.TimeZoneName,
//       meetingData.all_Day,
//       meetingData.initiativeId,
//       meetingData.google_meeting_id
//     ]
//     return await pool.query(qu, dataForUpdate);
//   } catch (error) {
//     throw error;
//   }
// }