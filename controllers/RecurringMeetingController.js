var meetingController = require('../controllers/MeetingController');
const WEEK_DAYS = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
const recurringMeetingService = require('../services/RecurringMeetingSerices');
const axios = require('axios');
const constants = require("../config/constants");
const util = require('util')
const timeZoneDetailsService = require('../services/timeZoneDetailsService');
const sqsService = require('../services/sqsService');
const meetingService = require('../services/MeetingServices');

let currentDate;
let userDetails;
/**
 * Following method is called to create or upate an RM
 */
module.exports.saveRecurringMeetingData = async (event, userTokenDetails, responseData) => {
  console.log(`---------- RM Event Received : ${event['summary']} ---------------`);
  console.log(event);
  userDetails = userTokenDetails;
  let headers = {
    'Authorization': `Bearer ${userTokenDetails['access_token']}`
  };
  currentDate = new Date();

  if (event.status == constants.CONFIRMED) {
    
    /*********************************START OF STEP 1******************************************
     * STEP 1.1 : Check if the event data received is an RM instance or parent Recurring Meeting
     ******************************************************************************************/
    let isParentRm = event.recurrence ? true : false;

    if (isParentRm) {

      let recurringMeetingDetail = {};
      let calendarResourceRecurrence = event['recurrence'][0];
      let startDate = new Date();
      let endDate = new Date();
      let recurrenceData = {};

      /********** STEP 1.2 **********
       * Following code prepares the recurrence object. Recurrence details received are in string format
      Thus converting all the string details to Object format.*/
      if (calendarResourceRecurrence) {
        let recurrenceProperties = calendarResourceRecurrence.slice(6).split(";");
        recurrenceProperties.forEach(property => {
          objArr = property.split("=");
          key = objArr[0];
          val = objArr[1];
          recurrenceData[key] = val;
        });
      }
      console.log(recurrenceData);
      // startDate.setHours(00);
      // startDate.setMinutes(01);

      endDate.setHours(23);
      endDate.setMinutes(59);
  
      let instanceUrl = `/events/${event.id}/instances/?timeMin=${startDate.toISOString()}&timeMax=${endDate.toISOString()}&timeZone=UTC`;
      instanceUrl = constants.GOOGLE_CALENDAR_URL + instanceUrl;

      /****STEP 1.3 ********** :
       *  If Parent RM. Check if the same already exists in our DB or not.
       *  Following api call gets (one instance for today ) for particular event.*/
      let instancesReceived = await axios.get(instanceUrl, {
        headers
      });
      console.log(util.inspect(instancesReceived.data.items, false, null, true /* enable colors */));
      let existingRmDetails = await recurringMeetingService.checkIfRmExist(event.id);

      /**
       * ************************* START OF STEP : 2 *************************
       * Preparing the object to be saved in RM table ( whether on creation or updation )
       * **********************************************************************/

      recurringMeetingDetail['subject'] = event['summary'];
      recurringMeetingDetail['repeat_every'] = (recurrenceData['INTERVAL']) ? parseInt(recurrenceData['INTERVAL']) : 1;
      recurringMeetingDetail['frequency_type'] = recurrenceData['FREQ'].toLowerCase();

      if (recurringMeetingDetail['frequency_type'].toLowerCase() == constants.MONTHLY) {
        recurringMeetingDetail['frequency_type'] = constants.ABSOLUTE_MONTHLY;
      }
      if (recurringMeetingDetail['frequency_type'].toLowerCase() == constants.YEARLY) {
        recurringMeetingDetail['frequency_type'] = constants.ABSOLUTE_YEARLY;
      }
      recurringMeetingDetail['organizer'] = event['organizer']['email'];
      recurringMeetingDetail['day_index'] = constants.FIRST;
      recurringMeetingDetail['weekdays_selected'] = null;

      //If meeting is all day then set the start date starting with 00:00AM 
      //else with given date time and set is_all_day to Y/N and TimeZoneName
      if (event['start']['date']) {
        recurringMeetingDetail['is_all_day'] = 'Y';
        recurringMeetingDetail['TimeZoneName'] = null;
        recurringMeetingDetail['TimeZoneOffset'] = "+0";
        recurringMeetingDetail['start_date'] = new Date(event['start']['date'])
        recurringMeetingDetail['end_date'] = new Date(event['end']['date'])
      } else {
        recurringMeetingDetail['is_all_day'] = 'N';
        recurringMeetingDetail['TimeZoneName'] = event['start']['timeZone'],
          recurringMeetingDetail['TimeZoneOffset'] = await getTimeZoneOffsetValue(event['start']['timeZone']);
        recurringMeetingDetail['start_date'] = new Date(event['start']['dateTime']);
        recurringMeetingDetail['end_date'] = new Date(event['end']['dateTime'])
      }
      //If user has entered an end time to the recurring meting then if condition will be executed
      //else we set the end time to 10Years.
      if (recurrenceData['UNTIL']) {

        let meetingEndTime;
        let b = recurrenceData['UNTIL'];
        let dateString = b.slice(0, 4) + "-" + b.slice(4, 6) + "-" + b.slice(6, 8);

        (event['end']['date']) ?
          recurringMeetingDetail['end_date'] = new Date(dateString + 'T23:59:59.000Z') :
          (meetingEndTime = event['end']['dateTime'].slice(10),
            recurringMeetingDetail['end_date'] = new Date(dateString + meetingEndTime));

      } else {
        let meetingDate = recurringMeetingDetail['start_date'].toISOString().slice(0, 10);
        let endDate = new Date(meetingDate);
        endDate.setFullYear(endDate.getFullYear() + 10);

        (event['end']['date']) ?
          recurringMeetingDetail['end_date'] = new Date(endDate.toISOString().slice(0, 10) + 'T23:59:59.000Z') :
          (meetingEndTime = event['end']['dateTime'].slice(10),
            recurringMeetingDetail['end_date'] = new Date(endDate.toISOString().slice(0, 10) + meetingEndTime));

      }

      /**
       * Following if condition will only execute when the frequency is relative Monthly.
       ********** NOTE : FOR ABSOLUTE MONTHLY THERE WILL BE NO BYDAY FIELD ************* 
       *********** AND THE FREQUENCY_TYPE IS SELECTED TO MONTHLY ABOVE  *****************
       * Google doesn't classify as absolute and relative monthly. It only appends a numbeR with the weekday 
       * eg : "4SU" : meaning it will occur every 4th of Sunday. So we we process the data and save it as abosolute and relative monthly
       */
      if (recurrenceData['FREQ'].toLowerCase() == constants.MONTHLY && recurrenceData['BYDAY'] !== undefined) {
        let a = recurrenceData['BYDAY'];
        let dayIndex;
        let weekDay = "";
        // console.log(a.slice(0,a.indexOf(a.match(/\d+/g)[0]) +1), "  ", a.slice(a.indexOf(a.match(/\d+/g)[0]) +1))
        //Following expression using regexp gives us the number in string format.
        //Following expression in regexp gives weekday after th number.
        a.match(/\d+/g) ?
          (dayIndex = a.slice(0, a.indexOf(a.match(/\d+/g)[0]) + 1), weekDay = a.slice(a.indexOf(a.match(/\d+/g)[0]) + 1)) :
          (dayIndex = 0);

        recurringMeetingDetail['frequency_type'] = constants.RELATIVE_MONTHLY;
        recurringMeetingDetail['weekdays_selected'] = constants.WEEKDAYS[weekDay];
        recurringMeetingDetail['day_index'] = constants.DAY_INDEX[dayIndex];

      }

      /**
       * Following if condition will only execute when the frequency is NOT monthly
       * In this case 'BYDAY' field will only have weekdays and no number attached to it.
       */
      if (recurrenceData['FREQ'].toLowerCase() !== constants.MONTHLY && recurrenceData['BYDAY'] !== undefined) {
        recurringMeetingDetail['weekdays_selected'] = recurrenceData['BYDAY']
          .split(",") // This will make comma seperated string of weekdays as array of weekdays 
          .map(x => constants.WEEKDAYS[x]) //This will return array of weekdays with full name ; before : SU,MO ; After : Sunday, Monday
          .join(); //This will convert the array of strings to a single string as will be stored in the database

        //Sorting the above array //Write logic for sorting the array according to weekdays
        recurringMeetingDetail['weekdays_selected'] = sortWeekDays(recurringMeetingDetail['weekdays_selected']);
      }

      recurringMeetingDetail['countable'] = 1;
      // event['attendees'] ? (event['attendees'].find(attendee => attendee['email'] == constants.SM_EMAIL_ID) ? 1 : 0) : 0;

      /********************************* END OF STEP 2 *******************************/

      /********************************* START OF STEP 3 *****************************
       * Using the sata whether new or exisintg data. CREATE/ UPDATE the rm data
       *****************************************************************************/
      if (existingRmDetails) {
        console.log(`---------- Existing RM Subject : ${event['summary']} --------------------`);
        let isDateChanged = await checkIsDateChanged(existingRmDetails, recurringMeetingDetail);
        console.log(`Is Date Changed for ${event['summary']}: `, isDateChanged);
        /**
         * Following If consition is executed when any of these RM fields are changed : 
         * start_date, end_date , frequency , day_index , interval ( repeat_every), weekdays_selected
         * It sets the next meetingDate to be saved in RM Table (To be used by cron job)
         * It then cancels the existing rm instances related to this RM
         * It updates the RM table with new dates 
         * Checks if start date of meeting is today , then it will create a meeting 
         */
        if (isDateChanged) {
          let meetingStartDateWithOffset;
          /**
           * If next meeting is today , then set meetingStartDateWithOffset with nextMeeting date 
           * otherwise with calendar startDate
           */
          let isCertTriggerredForExistingMeeting = false;
          if (instancesReceived.data.items.length !== 0) {

            let instanceStartDate;
            (instancesReceived.data.items[0]['start']['dateTime']) ?
              instanceStartDate = new Date(instancesReceived.data.items[0]['start']['dateTime']) :
              instanceStartDate = new Date(instancesReceived.data.items[0]['start']['date']);

            meetingStartDateWithOffset = calculateDateTimeWithOffset(instanceStartDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
            if (meetingStartDateWithOffset.toDateString() == currentDate.toDateString()) {

              event.start.dateTime = instanceStartDate.toISOString();
              /** So next meeting date will be set considering today's instacne start date */
              recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);
              console.log(recurringMeetingDetail['next_meeting_date']);
            }

            //TO DO : CANCEL EXISTING RM INSTANCES AND STOP THEIR STEP FUNCTIONS ONLY
            //IF EXISTING MEETING CERT IS NOT TRIGGERRED

            //checking if for the existing meeting cert is triggered or not.            

            /******************** Send data to sqs ***********************************/
            let a = currentDate.toISOString().slice(0,10).split("-").join("");
            let b = "";
            
            if(existingRmDetails.is_all_day === 'N') {
              b = existingRmDetails.start_date.toISOString().slice(10).split(":").join("").slice(0,7);
            }

            let meetingId = instancesReceived.data.items[0].recurringEventId + "_" + a + (b.indexOf('T') == -1 && b !== "" ? 'T' : '')  + b + (b !== "" ? "Z" : "");
            isCertTriggerredForExistingMeeting = await meetingService.checkIfCertTrigForMeeting(meetingId);
            console.log(`------Is Certificate Triggrred for today's meeting : -------`, isCertTriggerredForExistingMeeting);

            let messageGroupID = userTokenDetails['email'];
            
            if(isCertTriggerredForExistingMeeting) {
              instancesReceived.data.items[0].id = meetingId;
              let sqs_type =  "normal_meeting";
              let meetData = {
                items: [],
                timeZone: instancesReceived.data.timeZone,
                summary: instancesReceived.data.summary
              };
              meetData.items.push(instancesReceived.data.items[0]);
              console.log(`-----------Updating today's Instance -----------`);
              sqsService.sendToSqs(sqs_type, meetData, messageGroupID);
              /******* End of Send data to sqs ***************************************/
            } else {
              let sqs_type = "cancelled_meeting";
              let sqs_data = meetingId;
              console.log(`-----------Cancelling today's Instance : ${sqs_data}`);
              sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
              /******* End of Send data to sqs ***************************************/
            }
            

          } else {

            recurringMeetingDetail['next_meeting_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
            while (recurringMeetingDetail.next_meeting_date < currentDate) {
              event.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail.next_meeting_date, recurringMeetingDetail['TimeZoneOffset']);
              recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);
            }
            meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);

          }

          if (recurrenceData['COUNT']) {
            let numberOfInstances = parseInt(recurrenceData['COUNT']);
            let lastCountDate = new Date(recurringMeetingDetail.next_meeting_date.toISOString());
            recurringMeetingDetail['end_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
            for (let i = 0; i < numberOfInstances; i++) {
              event.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail['end_date'], recurringMeetingDetail['TimeZoneOffset']);
              recurringMeetingDetail['end_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);
            }
            console.log(recurringMeetingDetail.end_date);
          }

          /*
          let stateMachineArn = await meetingRepository.cancelRmInstances(calendarResource.id);
          if(stateMachineArn !== undefined) {
            for(let i=0;i<stateMachineArn.length ; i ++) {
              await meetingController.stopStepFunction(stateMachineArn[i])
            }
          }
          */

          let rmDetails = await recurringMeetingService.updateRecurringMeetings(recurringMeetingDetail, existingRmDetails.recurring_meeting_id);

          // let meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
          let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);

          if ((currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime()) && instancesReceived.data.items.length !== 0 && !isCertTriggerredForExistingMeeting) {
            //Create a meeting
            console.log("-----------Creating updated meeting -------------");
            if (instancesReceived.data.items.length !== 0) {
              instancesReceived.data.items[0]['rm_id'] = event.id;
              instancesReceived.data.items[0]['iCalUId'] = event.iCalUId;
              //TO DO CREATE NEW RM INSTANCE/MEETING FOR THE UPDATED THE RECURRING MEETING

              /**************************** Send data to sqs **********************************/
              let sqsType = "normal_meeting";
              let messageGroupID = userTokenDetails['email'];
              let sqsData = instancesReceived.data;
              sqsService.sendToSqs(sqsType, sqsData, messageGroupID);
              /******************* End of Send data to sqs **********************************/

            }
          } else if(recurringMeetingDetail.is_all_day === 'Y' && currentDateWithOffset.toDateString() == meetingStartDateWithOffset.toDateString() && !isCertTriggerredForExistingMeeting) {
              //Create a aLL DAY meeting instance
              console.log("-----------Creating All day updated meeting -------------");
              if (instancesReceived.data.items.length !== 0) {
                instancesReceived.data.items[0]['rm_id'] = event.id;
                instancesReceived.data.items[0]['iCalUId'] = event.iCalUId;
                //TO DO CREATE NEW RM INSTANCE/MEETING FOR THE UPDATED THE RECURRING MEETING

                /**************************** Send data to sqs **********************************/
                let sqsType = "normal_meeting";
                let messageGroupID = userTokenDetails['email'];
                let sqsData = instancesReceived.data;
                sqsService.sendToSqs(sqsType, sqsData, messageGroupID);
                /******************* End of Send data to sqs **********************************/
              }
          }


        } else {

          recurringMeetingDetail['next_meeting_date'] = new Date(existingRmDetails.next_meeting_date);
          console.log("------- Updating RM : ", recurringMeetingDetail);
          rmDetails = await recurringMeetingService.updateRecurringMeetings(recurringMeetingDetail, existingRmDetails.recurring_meeting_id);
          if (rmDetails !== null && rmDetails !== undefined) {

            /**
             * Following piece of code gets the existing rm instances in meetings table with conditions : 
             * START_DATE >= Today' date with time : TODAY'S_DAY-00:00:00 AND rm_id = calendarResource.id
             */
            let startDate = new Date();
            // startDate.setHours(00);
            // startDate.setMinutes(00);
            let scheduledRmInstances = [];
            scheduledRmInstances = await meetingService.getExistingInstances(existingRmDetails.resource_id, startDate.toISOString());

            if (scheduledRmInstances.length !== 0) {
              console.log("Schedule Instance for today is : ", scheduledRmInstances.id)
              /**
               * In most cases this for loop will run only once and instances array will consist only one instance for
               * today's date. As precautionary writing this for loop.
               */
              for (let i = 0; i < instancesReceived.data.items.length; i++) {
                /** Following code checks of the instance ( meeting to occur today ) is found in scheduledRMInstances
                 * Then it will update that instance in meeting_table also its corresponsing meeting_invitees table.
                 */
                console.log("---------- Instacne Received attendees : ----------", instancesReceived.data.items[i]['attendees']);
                console.log("---------Update meeting table for this instance ---------");

                instancesReceived.data.items[i]['iCalUId'] = existingRmDetails.outlook_meeting_id;
                let meetData = {
                  items: [],
                  timeZone: instancesReceived.data.timeZone,
                  summary: instancesReceived.data.summary
                };
                meetData.items.push(instancesReceived.data.items[i]);
                //TO DO UPDATE MEETING
                /********* Send data to sqs ****************************************************/
                var sqs_type = "normal_meeting";
                var messageGroupID = userTokenDetails['email'];
                var sqs_data = meetData;
                sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
                /******* End of Send data to sqs **********************************************/


              }
              if (instancesReceived.data.items.length == 0) {
                for (let i = 0; i < scheduledRmInstances.length; i++) {
                  //TO DO CANCEL ALL SCHEDULED RM INSTANCES AND STOP THEIR STEP FUNCTIONS
                  /*
                  let stateMachineArn = await meetingRepository.cancelRmInstances(scheduledRmInstances[i].rm_id);
                  if(stateMachineArn !== undefined) {
                    for(let i=0;i<stateMachineArn.length ; i ++) {
                      await meetingController.stopStepFunction(stateMachineArn[i])
                    }
                  }
                  */
                }
              }
            } else {

              // if(currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime()) {
              // let currentDate = new Date();
              // console.log("-----------Creating updated meeting -------------");
              if (instancesReceived.data.items.length !== 0 && existingRmDetails.cancelled_at == null) {

                console.log("---------------Creating Today's Meeting ---------------");

                let todayMeetingDate;
                (instancesReceived.data.items[0]['start']['date']) ?
                  todayMeetingDate = new Date(instancesReceived.data.items[0]['start']['date']) :
                  todayMeetingDate = new Date(instancesReceived.data.items[0]['start']['dateTime']);

                console.log("Current Date Without Offset : ", currentDate, " Meeting Start Date Without Offset : ", todayMeetingDate);

                let meetingStartDateWithOffset = calculateDateTimeWithOffset(todayMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);
                let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
                console.log("Current Date With Offset : ", currentDateWithOffset, " Meeting Start Date With Offset : ", meetingStartDateWithOffset);
                if (currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime()) {
                  instancesReceived.data.items[0]['rm_id'] = existingRmDetails.resource_id
                  instancesReceived.data.items[0]['iCalUId'] = existingRmDetails.outlook_meeting_id;
                  //TO DO : CREATE MEETING / RM INSTANCE FOR THIS SECTION.
                  // meetingController.createMeeting(instances[0], userId);   
                }
              }
              // }
            }

            if (recurrenceData['COUNT']) {
              let numberOfInstances = parseInt(recurrenceData['COUNT']);
              let lastCountDate = new Date(recurringMeetingDetail.next_meeting_date.toISOString());
              recurringMeetingDetail['end_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
              for (let i = 0; i < numberOfInstances; i++) {
                event.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail['end_date'], recurringMeetingDetail['TimeZoneOffset']);
                recurringMeetingDetail['end_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);
              }
              console.log(recurringMeetingDetail.end_date);
            }
          }
        }


      } else {
        recurringMeetingDetail['outlook_meeting_id'] = event['iCalUID'];
        recurringMeetingDetail['resource_id'] = event['id'];

        console.log(`------- Creating RM with subject : ${event.summary} -------`);

        /**
         *  If next meeting is today , then set meetingStartDateWithOffset with nextMeeting date 
         *  otherwise with calendar startDate */
        let meetingStartDateWithOffset;
        if (instancesReceived.data.items.length !== 0) {
          let instanceStartDate;
          (instancesReceived.data.items[0]['start']['dateTime']) ?
            instanceStartDate = new Date(instancesReceived.data.items[0]['start']['dateTime']) :
            instanceStartDate = new Date(instancesReceived.data.items[0]['start']['date']);

          meetingStartDateWithOffset = calculateDateTimeWithOffset(instanceStartDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
          
          console.log(`----Meeting Start Date with offset : ${meetingStartDateWithOffset} and current Date is : ${currentDate}`);

          if (meetingStartDateWithOffset.toDateString() == currentDate.toDateString()) {
            console.log(`---------Setting Next Meeting Date considering today's meeting ------`);
            event.start.dateTime = new String(instanceStartDate.toISOString());
            /** So next meeting date will be set considering today's instacne start date */
            recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);

          }
        } else {

          recurringMeetingDetail['next_meeting_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
          while (recurringMeetingDetail.next_meeting_date < currentDate) {
            event.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail.next_meeting_date, recurringMeetingDetail['TimeZoneOffset']);
            recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);
          }
          meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);

        }

        if (recurrenceData['COUNT']) {
          let numberOfInstances = parseInt(recurrenceData['COUNT']);
          console.log(recurringMeetingDetail.end_date)
          recurringMeetingDetail['end_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.end_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
          for (let i = 0; i < numberOfInstances; i++) {
            event.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail['end_date'], recurringMeetingDetail['TimeZoneOffset']);
            recurringMeetingDetail['end_date'] = nextsetNextMeetingDate(event, recurringMeetingDetail);
          }
          recurringMeetingDetail.end_date = calculateDateTimeWithoutOffset(recurringMeetingDetail['end_date'], recurringMeetingDetail['TimeZoneOffset'])
          console.log(recurringMeetingDetail.end_date);
        }
        
        console.log(util.inspect(recurringMeetingDetail, false, null, true /* enable colors */));

        let rmDetails = await recurringMeetingService.saveRecurringMeetings(recurringMeetingDetail);
        // let rmDetails = null;

        //Done Meeting Part
        if (rmDetails !== null) {

          /**
           * Check if this is being converted from normal to RM. Check if witht the same outlook_id 
           * and start_date.TodateString() == currentDate.Tostrin() then cancel.
           */
          let toCancelTodayMeet = await meetingService.getMeetingByIcalUid(recurringMeetingDetail['outlook_meeting_id']);
          if(toCancelTodayMeet.isSent && instancesReceived.data.items.length !== 0) {
            console.log('-----Updating Normal Meeting------');
            instancesReceived.data.items[0].id = toCancelTodayMeet.meetingId;
            let sqs_type =  "normal_meeting";
            let messageGroupID = userTokenDetails['email'];
            let meetData = {
              items: [],
              timeZone: instancesReceived.data.timeZone,
              summary: instancesReceived.data.summary
            };
            meetData.items.push(instancesReceived.data.items[0]);
            console.log(`-----------Updating today's Instance -----------`);
            sqsService.sendToSqs(sqs_type, meetData, messageGroupID);
            /******* End of Send data to sqs ***************************************/
            
          } else {
            //Cancel Meeting
            console.log('-----Cancelling Normal Meeting------');
            /******************** Send data to sqs ***********************************/
            var sqs_type = "cancelled_meeting";
            var messageGroupID = userTokenDetails['email'];
            var sqs_data = toCancelTodayMeet.meetingId;
            sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
            /******* End of Send data to sqs ***************************************/
            // sendUpdateMeeting(event,responseData,userTokenDetails);
          }
          let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
          /**
           * Following condition checks if the start date of RM is today . Then create a meeting instance
           * in meetings table and corresponding meeting_invitees table.
           * Provide the rm_id for later reference ( updating , deleting )
           */
          if (recurringMeetingDetail['is_all_day'] === 'N' && currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime() && !toCancelTodayMeet.isSent) {
            //Create a meeting
            if (instancesReceived.data.items.length !== 0) {
              console.log("-----------------Creating with Instace Details --------------------", instancesReceived.data.items[0]['start']);
              instancesReceived.data.items[0]['rm_id'] = event.id;
              instancesReceived.data.items[0]['iCalUId'] = event.iCalUId;


              /**************************** Send data to sqs **********************************/
              let sqsType = "normal_meeting";
              let messageGroupID = userTokenDetails['email'];
              let sqsData = instancesReceived.data;
              sqsService.sendToSqs(sqsType, sqsData, messageGroupID);
              /******************* End of Send data to sqs **********************************/
            } 
          }
          else if(recurringMeetingDetail['is_all_day'] === 'Y' && currentDateWithOffset.toDateString() == meetingStartDateWithOffset.toDateString() && !toCancelTodayMeet.isSent){
            console.log("-----------------Creating All Day Instace Details --------------------", instancesReceived.data.items[0]['start']);
            instancesReceived.data.items[0]['rm_id'] = event.id;
            instancesReceived.data.items[0]['iCalUId'] = event.iCalUId;


            /**************************** Send data to sqs **********************************/
            let sqsType = "normal_meeting";
            let messageGroupID = userTokenDetails['email'];
            let sqsData = instancesReceived.data;
            sqsService.sendToSqs(sqsType, sqsData, messageGroupID);
            /******************* End of Send data to sqs **********************************/
        }
        }

      }

    } else {
      console.log(`-------Handling Particular Instance Update ----------`);
      //check if meeting is of today

      let instanceStartTime = new Date(event.start.dateTime);
      let instanceEndTime = new Date(event.end.dateTime);
      let instanceStartTimeWithOffset;
      let timeZoneName = event.start.timeZone;
      let timeZoneOffset = await getTimeZoneOffsetValue(event['start']['timeZone']);
      let isAllDay = 'N';

      //If all day meeting ( instance ) update the details accordingly
      if(event.start.date) {
        isAllDay = 'Y';
        instanceStartTime = new Date(event.start.date);
        instanceEndTime = new Date(event.end.date);
        timeZoneName = null;
        timeZoneOffset = "+0";
      }
   
      instanceStartTimeWithOffset = calculateDateTimeWithOffset(instanceStartTime.getTime(), timeZoneOffset);

      if(isAllDay === 'Y' && (instanceStartTimeWithOffset.toDateString() == currentDate.toDateString())) {
        console.log(`------- Handling Update of All day event for today------`);
        sendUpdateMeeting(event, responseData, userTokenDetails);

      } else if ((instanceStartTimeWithOffset.toDateString() == currentDate.toDateString()) && (instanceStartTimeWithOffset.getTime() > currentDate.getTime()) && isAllDay === 'N') {
        //Update the Meeting instance
        console.log(instanceStartTimeWithOffset.getTime(), " ", currentDate.getTime());
        console.log(`--------Instance Updated for today's Date ---------`, instanceStartTime);
        sendUpdateMeeting(event, responseData, userTokenDetails);

      } else if(isAllDay === 'Y' && (instanceStartTimeWithOffset.toDateString() !== currentDate.toDateString())) {
        console.log(`------- Handling Update of all day event for either past or future ------`);
        saveRecurringMeetingActivity(event, instanceStartTime, instanceEndTime);
      } else {
        console.log(`------- Instance Updated for either past or future ; Date : -------`, instanceStartTime);        
        saveRecurringMeetingActivity(event, instanceStartTime, instanceEndTime);
      }
    }
  }
  else if (event.status == constants.CANCELLED) {

    console.log(util.inspect(event, false, null, true));
    try {
      let cancelledInstanceUrl = constants.GET_EVENTS + event.id + "?&timeZone=UTC";
      let cancelledInstance = await axios.get(cancelledInstanceUrl, {
        headers
      });

      console.log(util.inspect(cancelledInstance.data, false, null, true));

      let timeZoneOffset = (cancelledInstance.data['start']['date']) ? "+0" : await getTimeZoneOffsetValue(cancelledInstance.data['start']['timeZone']);
      let currentDate = new Date();
      let meetingStartDate = cancelledInstance.data.start.date ? new Date(cancelledInstance.data.start.date) : new Date(cancelledInstance.data.start.dateTime);
      let meetingEndDate = cancelledInstance.data.end.date ? new Date(cancelledInstance.data.end.date) : new Date(cancelledInstance.data.end.dateTime);
      let meetingStartDateWithOffset = calculateDateTimeWithOffset(meetingStartDate.getTime(), timeZoneOffset);
      let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), timeZoneOffset);

      //Whether cancelling is all day meeting or otherwise
      if (cancelledInstance.data['start']['date']) {

        if (meetingStartDateWithOffset.toDateString() == currentDate.toDateString()) {
          console.log("----Cancelling an all day instance of RM for today -----");
           /******************** Send data to sqs ***********************************/
           var sqs_type = "cancelled_meeting";
           var messageGroupID = userTokenDetails['email'];
           var sqs_data = cancelledInstance.data.id;
           sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
           /******* End of Send data to sqs ***************************************/
        } else {
          console.log("-------Cancelled all day instance of Either Past or Future instance of RM. Thus not dealing it-------");
        }


      } else {
        if (currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime() && meetingStartDateWithOffset.toDateString() == currentDate.toDateString()) {
          console.log("----Cancelling day instance of RM for today -----");

          //TO DO: CALL MEETING FUNCTION TO CANCEL THIS MEETING
          /******************** Send data to sqs ***********************************/
          var sqs_type = "cancelled_meeting";
          var messageGroupID = userTokenDetails['email'];
          var sqs_data = cancelledInstance.data.id;
          sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
          /******* End of Send data to sqs ***************************************/


        } else {
          console.log("-------Cancelled instance of Either Past or Future instance of RM. Thus not dealing it-------");
        }
      }

    } catch (error) {
      console.log("-----Error Occurred while cancelling rm instance -----", error);
    }


  }

}

/**
 * 
 * @param {*} event 
 * @param {*} responseData 
 * @param {*} userTokenDetails 
 */
function sendUpdateMeeting(event, responseData, userTokenDetails) {
  let meetData = {
    items: [],
    timeZone: responseData.timeZone,
    summary: responseData.summary
  }
  meetData.items.push(event);
  //TO DO UPDATE MEETING
  /********* Send data to sqs ****************************************************/
  let sqs_type = "normal_meeting";
  let messageGroupID = userTokenDetails['email'];
  let sqs_data = meetData;
  sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
  /******* End of Send data to sqs **********************************************/
}

/**
 * 
 * @param {*} event 
 * @param {*} instanceStartTime 
 * @param {*} instanceEndTime 
 */
async function saveRecurringMeetingActivity(event, instanceStartTime, instanceEndTime) {
  let parentData = await recurringMeetingService.checkIfRmExist(event.recurringEventId);
        
  let originalTime = event.originalStartTime.dateTime ? new Date(event.originalStartTime.dateTime) : new Date(event.originalStartTime.date);
  console.log(`-------Original Start Time ----`,originalTime);
  let parentRmStartTime = parentData.start_date ? new Date(parentData.start_date) : new Date(parentData.start_date);
  //only when its a future instance
  if(instanceStartTime.getTime() > currentDate.getTime() && instanceStartTime.toDateString() !== originalTime.toDateString()) {
    console.log(`------- Instance updated is a future event of the recurring meeting series -------`);
    //Add the entry in rm activity table
    console.log(`-------Saving details in RM Activity Table : ${instanceStartTime.toDateString()} ------`);
    let recurringMeetingInstanceActivity = {
      recurring_meetings_id : parentData.recurring_meeting_id,
      outlook_meeting_id : event.iCalUID,
      meeting_date: originalTime,
      meeting_start: instanceStartTime,
      meeting_end: instanceEndTime,
      is_deleted : event.status == 'cancelled' ? 1 : 0
    }
    recurringMeetingService.saveRecurringMeetingActivity(recurringMeetingInstanceActivity);
  }

  /** Following if condition is to handle scenarios when the original time is today ( current date ) ; then delete today's
   * instance from the meetings table. 
   * ***NOTE : This method is only executed when any future/past meeting is changed
   * and below confition is only executed when today's meeting is changed to either past or future. delete this meeting **
   */
  if(originalTime.toDateString() == currentDate.toDateString() && originalTime.getTime() > currentDate.getTime()) {
    console.log(`------- Cancelling today's meeting from meetings table -------`);
    /******************** Send data to sqs ***********************************/
    var sqs_type = "cancelled_meeting";
    var messageGroupID = userDetails['email'];
    var sqs_data = event.id;
    sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
    /******* End of Send data to sqs ***************************************/
  }
}

/**
 * Following method sends a boolean value whether any field related to dates, frequency is changed 
 * Those fields which play a role in deciding when is the next meeting date going to occur.
 * @param {*} existingRmData 
 * @param {*} incomingRmData 
 */
function checkIsDateChanged(existingRmData, incomingRmData) {
  if (existingRmData !== null && incomingRmData !== null) {

    if (existingRmData.start_date.getTime() !== incomingRmData.start_date.getTime()) {
      console.log(`----Start Date is changed to : ${incomingRmData.start_date} from : ${existingRmData.start_date}---`);
      return true;
    } else if (existingRmData.end_date.getTime() !== incomingRmData.end_date.getTime()) {
      console.log(`----End Date is changed to : ${incomingRmData.end_date} from : ${existingRmData.end_date}---`);
      return true;
    } else if (existingRmData.repeat_every !== incomingRmData.repeat_every) {
      console.log(`----Interval is changed to : ${incomingRmData.repeat_every} from : ${existingRmData.repeat_every} ----`);
      return true;
    } else if (existingRmData.frequency_type !== incomingRmData.frequency_type) {
      console.log(`---Frequency Type is changed to ${incomingRmData.frequency_type} from ${existingRmData.frequency_type} ---`);
      return true;
    } else if (existingRmData.day_index !== incomingRmData.day_index) {
      console.log(`--- Day Index changed to ${incomingRmData.day_index} from ${existingRmData.day_index} ----`);
      return true;
    } else if (existingRmData.weekdays_selected !== incomingRmData.weekdays_selected) {
      console.log(`--- Weekdays Selected changed to ${incomingRmData.weekdays_selected} from ${existingRmData.weekdays_selected} ---`);
      return true
    } else return false;

  } else return false;
}

/**
 * Following method accept these two params and provides an output of time with offset added
 * @param {*} nextMeetingDateTime 
 * @param {*} timeZoneOffset 
 */
function calculateDateTimeWithOffset(nextMeetingDateTime, timeZoneOffset) {

  let totalMinutes = 0;
  let totalSeconds = 0;
  let sign = timeZoneOffset.slice(0, 1); //This can be either + or -
  let timeToAddSubtract = new String(timeZoneOffset.slice(1));


  if (timeToAddSubtract.includes(":") || timeToAddSubtract.includes(".")) {
    let hour = parseInt(timeToAddSubtract.slice(0, timeToAddSubtract.indexOf(":")));
    let minutes = parseInt(timeToAddSubtract.slice(timeToAddSubtract.indexOf(":") + 1));

    totalSeconds = ((hour * 60) + minutes) * 60; //Converting to seconds
  } else {
    totalSeconds = timeToAddSubtract * 60 * 60; //Converting Hours to seconds
  }
  if (sign == "?") {
    return new Date(nextMeetingDateTime - (totalSeconds * 1000));
  } else {
    return new Date(nextMeetingDateTime + (totalSeconds * 1000));
  }

}

/**
 * 
 * @param {*} dateTime 
 * @param {*} timeZoneOffset 
 */
function calculateDateTimeWithoutOffset(dateTime, timeZoneOffset) {
  let sign = timeZoneOffset.slice(0, 1); //This can be either + or -
  let totalSeconds = getTimeZoneOffsetSeconds(timeZoneOffset);
  if (sign == "?") {
    return new Date(dateTime + (totalSeconds * 1000));
  } else {
    return new Date(dateTime - (totalSeconds * 1000));
  }
}
/**
 * 
 * @param {*} timeZoneOffset 
 */
function getTimeZoneOffsetSeconds(timeZoneOffset) {

  let timeToAddSubtract = new String(timeZoneOffset.slice(1));
  if (timeToAddSubtract.includes(":") || timeToAddSubtract.includes(".")) {
    let hour = parseInt(timeToAddSubtract.slice(0, timeToAddSubtract.indexOf(":")));
    let minutes = parseInt(timeToAddSubtract.slice(timeToAddSubtract.indexOf(":") + 1));
    return ((hour * 60) + minutes) * 60; //Converting to seconds
  } else {
    return totalSeconds = timeToAddSubtract * 60 * 60; //Converting Hours to seconds
  }
}

/**
 * Following method calculates the next meeting date for that rm .
 * There are 6 cases : WEEKLY , DAILY , ABSOLUTE MONTHLY , RELATIVE MONTHLY , ABSOLUTE YEARLY AND RELATIVE YEARLY
 * Based on any of above frequency_type , we calculate the next meeting date with the offset added to it.
 * @param {*} event 
 * @param {*} timeZoneOffset 
 */
function nextsetNextMeetingDate(event, recurringMeetingDetail) {

  let index = recurringMeetingDetail['day_index'];
  let interval = recurringMeetingDetail['repeat_every'];

  let startDay = event.start.dateTime;
  let nextMeetingDate = new Date(startDay);
  console.log("nextMeetingDate , ", nextMeetingDate, " startDay , ", startDay, " interval ", interval)

  if (recurringMeetingDetail['frequency_type'] == constants.WEEKLY) {

    let weekdaysSelected = recurringMeetingDetail['weekdays_selected'] ? recurringMeetingDetail['weekdays_selected'].split(",") : ['data'];

    if (weekdaysSelected.length == 1) {

      nextMeetingDate.setDate(nextMeetingDate.getDate() + 7 * interval);
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);

    } else {

      let meetingDay = WEEK_DAYS[nextMeetingDate.getDay()];
      console.log(`--------meeting day : ${meetingDay}-----------------`);


      if (weekdaysSelected.indexOf(meetingDay) == (weekdaysSelected.length - 1)) {
        //Set next meeting date after weeks from 1st day in array.
        let gapBwFirstnLastMeetingInWeek = WEEK_DAYS.indexOf(meetingDay) - WEEK_DAYS.indexOf(weekdaysSelected[0])
        let repeatMeetingAfter = (interval - 1) * 7 + (7 - gapBwFirstnLastMeetingInWeek);

        nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
        return calculateDateTimeWithOffset(nextMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);

      } else {
        //Set next meeting day for next day in weekDaysSelected array.
        console.log("------Weekdays selected are : ------", weekdaysSelected);
        let meetingDayIndex = weekdaysSelected.indexOf(meetingDay); //This will give index of day in weekDaysSelected array in DB

        //Following code will give the week day number (b)
        let meetingWeekDay = WEEK_DAYS.indexOf(meetingDay);
        let nextMeetingWeekDay = WEEK_DAYS.indexOf(weekdaysSelected[meetingDayIndex + 1]);

        let repeatMeetingAfter;
        (nextMeetingWeekDay < meetingWeekDay) ?
          (repeatMeetingAfter = 7 - meetingWeekDay + nextMeetingWeekDay) : (repeatMeetingAfter = nextMeetingWeekDay - meetingWeekDay);

        console.log(`--------nextMeetingWeekDay : ${nextMeetingWeekDay} repeatMeetingAfter : ${repeatMeetingAfter} -----------------`);

        nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
        return calculateDateTimeWithOffset(nextMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);
      }
    }


  } else if (recurringMeetingDetail['frequency_type'] == constants.ABSOLUTE_MONTHLY) {

    nextMeetingDate.setMonth(nextMeetingDate.getMonth() + interval);
    return calculateDateTimeWithOffset(nextMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);

  } else if (recurringMeetingDetail['frequency_type'] == constants.RELATIVE_MONTHLY) {

    let dayIndex = nextMeetingDate.getDay();
    let monthIndex = nextMeetingDate.getMonth();
    let nextMonthIndex = monthIndex + interval;

    let firstDayOfNextMonth = new Date(nextMeetingDate.getFullYear(), nextMonthIndex, 1);
    firstDayOfNextMonth.setHours(nextMeetingDate.getHours());
    firstDayOfNextMonth.setMinutes(nextMeetingDate.getMinutes());
    // console.log("FIrst day of next month ", firstDayOfNextMonth);

    if (index == "first") {
      if (dayIndex == firstDayOfNextMonth.getDay()) {
        // return firstDayOfNextMonth;
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime() + recurringMeetingDetail['TimeZoneOffset']);
      } else {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        // return firstDayOfNextMonth;
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), recurringMeetingDetail['TimeZoneOffset']);
      }
    }
    if (index == "second") {
      while (firstDayOfNextMonth.getDay() !== dayIndex) {
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
      }
      firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7)
      return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }
    if (index == "third") {
      while (firstDayOfNextMonth.getDay() !== dayIndex) {
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
      }
      firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 2);
      return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }
    if (index == "fourth") {
      while (firstDayOfNextMonth.getDay() !== dayIndex) {
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
      }
      firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 3);
      return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }
    if (index == "last") {
      let lastDayOfNextMonth = new Date(firstDayOfNextMonth.getFullYear(), firstDayOfNextMonth.getMonth() + 1, 0, firstDayOfNextMonth.getHours(), firstDayOfNextMonth.getMinutes(), firstDayOfNextMonth.getSeconds());
      // console.log("Last Day of Next Month is : ", lastDayOfNextMonth);
      while (lastDayOfNextMonth.getDay() !== dayIndex) {
        lastDayOfNextMonth.setDate(lastDayOfNextMonth.getDate() - 1);
      }
      // return lastDayOfNextMonth;
      return calculateDateTimeWithOffset(lastDayOfNextMonth.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }

  } else if (recurringMeetingDetail['frequency_type'] == constants.DAILY) {
    nextMeetingDate.setDate(nextMeetingDate.getDate() + interval)
    return calculateDateTimeWithOffset(nextMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);

  } else if (recurringMeetingDetail['frequency_type'] == "relativeYearly") {

    let dayIndex = nextMeetingDate.getDay();

    let firsDayOfSameMonthNextYear = new Date(nextMeetingDate.getFullYear() + 1, nextMeetingDate.getMonth(), 1);
    firsDayOfSameMonthNextYear.setHours(nextMeetingDate.getHours());
    firsDayOfSameMonthNextYear.setMinutes(nextMeetingDate.getMinutes());
    firsDayOfSameMonthNextYear.setSeconds(nextMeetingDate.getSeconds());

    if (index == "first") {
      if (dayIndex == firsDayOfSameMonthNextYear.getDay()) {
        // return firsDayOfSameMonthNextYear;
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), recurringMeetingDetail['TimeZoneOffset']);
      } else {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        // return firsDayOfSameMonthNextYear;
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), recurringMeetingDetail['TimeZoneOffset']);
      }
    }
    if (index == "second") {
      while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
      }
      firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7);
      return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }
    if (index == "third") {
      while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
      }
      firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 2);
      return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }
    if (index == "fourth") {
      while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
      }
      firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 3);
      return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }
    if (index == "last") {
      let lastDayOfSameMonthNextYear = new Date(firsDayOfSameMonthNextYear.getFullYear(), firsDayOfSameMonthNextYear.getMonth() + 1, 0, firsDayOfSameMonthNextYear.getHours(), firsDayOfSameMonthNextYear.getMinutes(), firsDayOfSameMonthNextYear.getSeconds());
      // console.log("Last Day of sam Month next year is : ", lastDayOfSameMonthNextYear);
      while (lastDayOfSameMonthNextYear.getDay() !== dayIndex) {
        lastDayOfSameMonthNextYear.setDate(lastDayOfSameMonthNextYear.getDate() - 1);
      }
      lastDayOfSameMonthNextYear;
      return calculateDateTimeWithOffset(lastDayOfSameMonthNextYear.getTime(), recurringMeetingDetail['TimeZoneOffset']);
    }

  } else if (recurringMeetingDetail['frequency_type'] == "absoluteYearly") {
    nextMeetingDate.setFullYear(nextMeetingDate.getFullYear() + interval);
    return calculateDateTimeWithOffset(nextMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);
  }
}

/**
 * Following method will be called to get the timezone offset value from time_zone_Details table.
 * It accepts the timezone name eg : India Standard TIme and provide offset wrt to URC
 * @param {*} timeZoneName 
 */
async function getTimeZoneOffsetValue(timeZoneName) {
  let timeZoneOffset = await timeZoneDetailsService.getTimeZoneOffsetValue(timeZoneName);
  return timeZoneOffset;
}

/**
 * To sort array of weekdays starting from sunday and in order if not in order
 */
function sortWeekDays(weekdaysArray) {
  weekdaysArray = weekdaysArray.split(",");
  if(weekdaysArray.length > 1) {
    console.log("------Sorting weekdays ---- : ", weekdaysArray);
    let weekdays = Object.values(constants.WEEKDAYS);
    let weekdaysInNumber = weekdaysArray.map(weekday => weekdays.indexOf(weekday)).sort();
    return weekdaysInNumber.map(weekdayNumber => weekdays[weekdayNumber]).join();

  } else return weekdaysArray.join();
}


/**
 * Following method accepts resourceId ( message.resourceData.id ) and helps us identify whethere 
 * this resource is an RM or not. 
 */
module.exports.checkIfRm = async (resourceId) => {

  let isRm = await recurringMeetingService.checkIfRm(resourceId);
  console.log(isRm);
  return isRm;

}

/**
 * Following function accepts resource_id ( rmId ) and cancels the recurring meeting 
 * by setting the cancellattion date and furthere deleting rm instances in meeting table related to it
 * Also in the end deleted meeting invitees for those instances deleted
 */
module.exports.cancelRecurringMeeting = async (event, userTokenDetails) => {
  console.log("--------------------------Cancelling Recurring Meeting --------------------------");
  let removeData = await recurringMeetingService.cancelRecurringMeeting(event.id);
  if(removeData) {
      
    let todaysScheduledMeetingData = await meetingService.getExistingInstances(event.id);
    if(todaysScheduledMeetingData) {
      console.log(`-----Today Meeting id to be cancelled as a part of cancelled al series : ----------${todaysScheduledMeetingData.meeting_id}`)

      //Cancel this meeting from meetings table
      /******************** Send data to sqs ***********************************/
      let sqs_type = "cancelled_meeting";
      let messageGroupID = userTokenDetails['email'];
      let sqs_data = todaysScheduledMeetingData.meeting_id;
      sqsService.sendToSqs(sqs_type, sqs_data, messageGroupID);
      /******* End of Send data to sqs ***************************************/
    }
    
  

  }
  return true;

}



//If cert is already triggered
//An update of an instance .  

