const { pool } = require("../models/model");
const authServices = require('../services/AuthServices');
const axios = require('axios');

/**
 * Following method check the user exist with google tokens for apps script
 */
module.exports.checkAccess = async (req, res) => {
  try {
    console.log("current user from apps script", req.body);

    let emailId = req.body.email;
    let accessData = await authServices.checkAppScritAccess(emailId);
    if (accessData.length > 0 && accessData[0].auth_code && accessData[0].access_token && accessData[0].refresh_token) {
      return res
        .status(200)
        .json({ success: true, message: "Fetched Successfully", data: accessData, access: true });
    } else {
      return res
        .status(200)
        .json({ success: true, message: "Auth Needed", data: accessData, access: false });
    }

  } catch (e) {
    return res
      .status(500)
      .json({ success: false, message: "Unexpected error", data: {}, access: false });
  }
};

/**
 * Following method gives user details based on email id.
 */
module.exports.userDetailsByEmailId = (req, res) => {

  let getUserByEmailIdQuery = "select * from users where `email` = ? "
  let emailId = req.params.emailId;

  try {

    let result = pool.query(getUserByEmailIdQuery, [emailId]);
    console.log("Successfully got user details ");
    res.status(200).send(result);

  } catch {

    console.log("Error occurred while getting user details : ", error);
    res.status(500).json({ Message: "Error Occurred while getting user details" });

  };
};


/**
 * Following method check the user and return user Data
 */
module.exports.getUserData = async (req, res) => {
  try {

    let emailId = req.body.email;

    let resUserdata = await authServices.getUserDataByEmail(emailId);

    if (resUserdata.length > 0) {
      return res
        .status(200)
        .json({ success: true, message: "Successfull", data: resUserdata, access: true });
    } else {
      return res
        .status(200)
        .json({ success: true, message: "Auth Needed", data: resUserdata, access: false });
    }
  } catch (e) {
    return res
      .status(500)
      .json({ success: false, message: "Unexpected error", data: {}, access: false });
  }
};