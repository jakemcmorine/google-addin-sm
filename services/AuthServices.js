const jwt = require('jsonwebtoken');
const { pool } = require('../models/model');

const clientId = process.env.CLIENT_ID;

class AuthService {

    verifyToken = async (token) => {
        try {
            const { azp, email } = jwt.decode(token);
            if (azp === clientId) {
                return email;
            } else {
                console.error("401: Token source not varified");
                throw "401: Token source not varified";
            }
        } catch (error) {
            throw error;
        }
    }

    /**
     * Following function to save or update the incoming user tokens form apps script
     */
    saveOrUpdateUserToken = async (data, action) => {
        try {
            if (action == 'create') {

                const { email, auth_code, access_token, refresh_token, webhookId, expirationTime, resourceId } = data;
                const qu = `INSERT INTO google_user_tokens (email, auth_code, access_token, refresh_token, 
                            webhookId,subscriptionTime, expirationTime, resourceId) VALUES (?, ?, ?,?, ?,?, ?, ?)`;
                await pool.query(qu, [email.toLowerCase(), auth_code, access_token, refresh_token, webhookId, new Date().toISOString(), expirationTime, resourceId]);

            } else if (action == 'update') {

                const { email, auth_code, access_token, refresh_token, webhookId, expirationTime, resourceId } = data;
                const qu = `UPDATE google_user_tokens SET auth_code=?,access_token=?,
                    refresh_token=?, webhookId =?, subscriptionTime=?, expirationTime=? , resourceId =? WHERE email = ?`;
                await pool.query(qu, [auth_code, access_token, refresh_token, webhookId, new Date().toISOString(), expirationTime, resourceId, email]);

            } else if (action == 'updateTokens') {

                const { email, auth_code, access_token, refresh_token } = data;
                const qu = `UPDATE google_user_tokens SET auth_code=?,access_token=?,refresh_token=? WHERE email = ?`;
                await pool.query(qu, [auth_code, access_token, refresh_token, email]);

            } else if (action == 'updateAccessTokensWithEmail') {
                const { access_token, email } = data;
                const qu = `UPDATE google_user_tokens SET access_token=? WHERE email = ?`;
                await pool.query(qu, [access_token, email]);
            
            } else if(action == "updateSyncToken") {
                const { nextSyncToken, email } = data;
                const query = `update google_user_tokens SET syncToken=? WHERE email = ?`;
                await pool.query(query, [nextSyncToken, email]);
            }

            return true;
        } catch (error) {
            console.log("Error Occurred while performing : " + action + " : saving or updating function : ", error);
        }
    }

    /**
     * Following function to check the apps script access
     */

    checkAppScritAccess = async (emailId) => {
        try {
            const checkQuery = `select g.*,u.id as userId,u.firstName,u.lastName from google_user_tokens g left join users u on g.email = u.email where g.email=?`;
            let resultData = await pool.query(checkQuery, [emailId]);
            return resultData;
        } catch (error) {
            throw error;
        }
    }

    /**
     * Following function will provide google user token details based on webhookId.
     * THis will be called on notification receieved on our channel to further use access token to get data
     */
    getUserTokenDetails = async (webhookId) => {
        try {
            let query = 'select * from google_user_tokens where webhookId = ?';
            let userDetails = await pool.query(query, [webhookId]);
            return userDetails[0];
        } catch (error) {
            console.log("Error Occurred while getting user token details : ", error);
        }
    }

    /**
     * Following function to get user datas
     */

    getUserDataByEmail = async (emailId) => {
        try {
            const queryData = `select * from users where email=?`;
            let resultData = await pool.query(queryData, [emailId]);
            return resultData;
        } catch (error) {
            throw error;
        }
    }

}

module.exports = new AuthService();