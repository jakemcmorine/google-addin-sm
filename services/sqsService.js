var aws             = require('aws-sdk');

var accessKeyId     = CONFIG.SQS_ACCESS_KEY_ID;
var secretAccessKey = CONFIG.SQS_SECRET_ACCESS_KEY;
var region          = CONFIG.SQS_REGION;
var queueUrl        = CONFIG.SQS_URL;

var sqs = new aws.SQS({"accessKeyId":accessKeyId, "secretAccessKey": secretAccessKey, "region": region});



module.exports.sendToSqs = async(sqs_type,sqs_data,sqs_group, delay) =>
{ 
  console.log("Inside sendToSqs")
  var type = sqs_type ? sqs_type : "";
  var data = sqs_data ? sqs_data : {};
  var sqsMessageGroup =  sqs_group? sqs_group : "";
  let randNum = Math.floor(Math.random() * (10000 - 99999 + 1)) + 99999;
  var MsgDeduplicationId = "m-"+randNum+new Date().getTime();
  var delay_time = delay ? delay : 0;
  var params = {
    MessageBody: JSON.stringify({"type": type ,"data":data}),
    QueueUrl: queueUrl,
    MessageAttributes: {
      someKey: { DataType: 'String', StringValue: "string"}
    },
    MessageGroupId: sqsMessageGroup,
    MessageDeduplicationId: MsgDeduplicationId
  };
  console.log("sqs params:",params);
  await timer(delay_time);// then the Promise can be awaited   
  sqs.sendMessage(params, function(err, data) {
    if (err){
      console.log("sendToSqs ERROR:",err) // an error occurred
      return false
    } 
    else{
      console.log("sendToSqs SUCCESS:",data);  // successful response
      return true
    }
        
  });  
}

function timer(ms) {
 return new Promise(res => setTimeout(res, ms));
}