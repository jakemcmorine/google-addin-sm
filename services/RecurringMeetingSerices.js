const { pool } = require("../models/model");


class RecurringMeetingService {

    /**
     * Following method receives the recurring meeting data object and saved it in RM table
     * @param {*} recurringMeetingDetails 
     */
    async saveRecurringMeetings(recurringMeetingDetails) {
        try {
            //location, start_day,
            const {subject, start_date, repeat_every, frequency_type, day_index, end_date, weekdays_selected,
            TimeZoneOffset,TimeZoneName,organizer, outlook_meeting_id,countable, next_meeting_date, resource_id,
            is_all_day, } = recurringMeetingDetails;

            let rmSaveQuery = `insert into recurring_meetings (calendar_type, start_date, end_date, is_all_day, 
                repeat_every,frequency_type, weekdays_selected, day_index, subject, TimeZoneOffset, TimeZoneName, organizer,
                outlook_meeting_id, countable, next_meeting_date, resource_id)
                values (?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,? )`;
                
            let result = await pool.query(rmSaveQuery, ['google',start_date,end_date, is_all_day, repeat_every, frequency_type,weekdays_selected,
             day_index,subject, TimeZoneOffset, TimeZoneName,organizer, outlook_meeting_id, countable, next_meeting_date, resource_id  ]);

            console.log("------RM Details saved with id  : --------", result.insertId);
            return result.insertId;
        }
        catch(error) {
            console.log("Error occurred while saving rm data ----------", error);
            return null;
        }
    }

    async updateRecurringMeetings(recurringMeetingDetails, id) {
        console.log("----------------Updating Recurring Meeting with Subject : ------------", recurringMeetingDetails.subject);
        try {
            const {subject, start_date, repeat_every, frequency_type, day_index, end_date, weekdays_selected,
                TimeZoneOffset,TimeZoneName,organizer,countable, next_meeting_date,
                is_all_day, } = recurringMeetingDetails;
    
            let rmSaveQuery = `UPDATE recurring_meetings SET start_date =?, end_date=?, is_all_day=?, 
                repeat_every=?,frequency_type=?, weekdays_selected=?, day_index=?, subject=?, TimeZoneOffset=?, TimeZoneName=?, organizer=?,
                countable=?, next_meeting_date=? WHERE recurring_meeting_id = ?`;
                
            let result = await pool.query(rmSaveQuery, [start_date,end_date, is_all_day, repeat_every, frequency_type,weekdays_selected,
                day_index,subject, TimeZoneOffset, TimeZoneName,organizer , countable, next_meeting_date, id  ]);

            console.log("------RM Details updated  : --------", result.affectedRows > 0 ? true : false);
            return result;

            // if(rowData !== null && rowData !== undefined ) {
            //     console.log("----------------Result after updating ------------ ", rowData.dataValues.recurring_meeting_id);
            //     return rowData.dataValues;
            // } else {
            //     return null;
            // }
            
        }
        catch(error) {  
            console.log(`Error Occurred while updating RM details with subject ${subject}: `, error);
            return null;
        }
    }

    async updateStartDateNmd(instanceStartDate, rmId) {

        console.log(`---- Updating RM : ${rmId} start date and nmd ----`);

        try {
            let updateQuery = 'update recurring_meetings set next_meeting_date = ? where recurring_meeting_id = ?';
            let result = await pool.query(updateQuery, [instanceStartDate, instanceStartDate, rmId]);
            
            console.log(`--------Update the start date of RM and set the same next meeting date ---------`, result);
            return result;
        }
        catch(error) {
            console.log(`Error Occurred while updating RM details`);
            return null;
        }


    }

    /**
     * 
     * @param {*} resourceId 
     */
    async saveRecurringMeetingActivity(recurringMeetingInstanceDetails) {
        try {
            //location, start_day,
            const {recurring_meetings_id, outlook_meeting_id, meeting_date, meeting_start, meeting_end, is_deleted} = recurringMeetingInstanceDetails;

            let rmSaveQuery = `insert into recurring_meeting_activities (recurring_meetings_id, outlook_meeting_id, meeting_date,
                meeting_start, meeting_end, is_deleted)
                values (?, ?, ?, ?, ?, ?)`;
                
            let result = await pool.query(rmSaveQuery, [recurring_meetings_id, outlook_meeting_id, meeting_date, 
            meeting_start, meeting_end, is_deleted ]);

            console.log(`------RM : ${recurring_meetings_id} Activity Details saved with id  : --------`, result.insertId);
            return result.insertId;
        }
        catch(error) {
            console.log("Error occurred while saving rm data ----------", error);
            return null;
        }
    }

    /**
     * Following method will check whether an RM with this resourceOd/id exists in db or not.
     * If yes returns true else returns false
     * @param {String} resourceId : String
     */
    async checkIfRmExist(resourceId) {
        console.log("----------------Checking if this is a recurring meeting or not ---------------");
        try {
            let checkRmQuery = "select * from recurring_meetings where resource_id = ? LIMIT 1";

            const rmExists = await pool.query(checkRmQuery, [resourceId]);;
            if(rmExists !== null && rmExists.length !==0) { 
                return rmExists[0];
            }
            else return null;
        }
        catch(error) {
            console.log("--- Error Occurred while checking if this is recurring meeting or not --- ", error);
        }
    }

    /**
     * Following method is executed to cancel a Recurring Meeting based on the 
     * resourceId received from webhook
     * @param {*} id 
     */
    async cancelRecurringMeeting(id) {
    try {
       let cancelQuery = "update recurring_meetings set cancelled_at = ? where resource_id = ? ";
       let cancelResult = await pool.query(cancelQuery, [new Date().toISOString(), id]);
       console.log(`-------Cancel Status for ${id} is ------`, cancelResult.affectedRows !== 0 ? true : false);
       return cancelResult.affectedRows !== 0 ? true : false;
    } catch(error) {
        console.log("Error Occurred while cancelling RM with id : " , id);
    }
       

    }

}

module.exports = new RecurringMeetingService();