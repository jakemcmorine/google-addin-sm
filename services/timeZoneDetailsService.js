const { pool } = require("../models/model");


// const constants = require('../config/constants');

class TimeZoneDetailsService {

    async getTimeZoneOffsetValue(timeZoneName) {
        
        let timeZoneQuery = "select * from time_zone_details where time_zone_name = ? LIMIT 1";
        let timeZoneOffset = await pool.query(timeZoneQuery, [timeZoneName]);
        
        if (timeZoneOffset && timeZoneOffset[0] !== undefined) {
            return timeZoneOffset[0]['time_zone_offset'];
        } else {
            return "+0";
        }
        
    }
    
}

module.exports = new TimeZoneDetailsService();