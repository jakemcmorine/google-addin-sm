const { pool } = require("../models/model");

class MeetingRepository {

    /**
     * Following method gives the count of number of meetings attended by the user.
     * based on userId get all meeting count where countable = 1 
     * @param {*String} fieldName 
     * @param {*} userId 
     */
    async meetingCount (fieldName, userId) {
        try {
            let meetingCountQuery = `select count(*) as count from meetings where ${fieldName} = ? and countable = 1`;
            const meetingCount = await pool.query(meetingCountQuery, [userId]);
            return meetingCount
        } catch(err) {
            console.log(`---Error Occurred while getting meeting count for user : ${userId} ---`);
            return 0;
        }
        
    }
    
    async meetingInviteCount (fieldName, userId) {
        try {
            // let treesPlantQuery = `select count(*) as count from meeting_invites where emailsent_date is not null 
                // and meeting_id in (SELECT id FROM meetings where ${fieldName} = ? ORDER BY id DESC)`
            let treesPlantQuery = `select SUM(trees) as count from tree_counts where ${fieldName} = ?`
            const treesPlantCount = await pool.query(treesPlantQuery, [userId]);
            return treesPlantCount;
        } catch (err) {
            console.log(err);
            console.log("---Error Occurred while meetingInviteCount ---");
            return false;
        }
    }


    async checkUserIp(userId) {
        let isUserIp = false;
        try {
            isUserIp = await pool.query("select * from meetings where org_id = ? LIMIT 1", [userId]);
            if(isUserIp !== null && isUserIp.length !==0) { 
                return true;
            }
            else return false;
        } 
        catch(err) {
            console.log("---Error Occurred while checking if user is an ip ---" , userId);
            return false;
        }

    }

    async meetingData(where) {
        
        let meeting = await meetingModel.findOne(where);
        return meeting
    }

    async removeMeeting(where) {
        let meeting = await meetingModel.destroy(where);
        return meeting
    }

    async removeInvitees(id) {
        let meeting = await meetingInviteeModel.destroy({ where: { meeting_id : id}});
        return meeting
    }

    async insertInvitees(data) {
        let meeting = await meetingInviteeModel.bulkCreate(data);
        return meeting
    }

    async updateMeeting(data, id) {
        try {
            let updateMeeting = await meetingModel.update(data, {
                where: { id: id },
            });
            return updateMeeting;
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    async createMeeting(data, invitees) {
        try {
            const meeting = await meetingModel.create(data);
            return meeting;
     
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    async getMeetingDetails(meetingId) {
        let meeting = await meetingModel.getMeetingDetails(meetingId);
        return meeting
    }

    /**
     * Get meeting based on rm-id and startdate as provided
     * @param {*} outlook_meeting_id 
     * @param {*} startDate 
     */
    async getExistingInstances(rmId) {
        try {
            console.log(`-----Getting today's meeting for rm id : ${rmId} and start date >= ${new Date().toISOString()}  ---------`);
            let findMeetingQuery = "select * from meetings where rm_id =? and start_date >= ?"
            let currentRmInstances = await pool.query(findMeetingQuery, [rmId, new Date().toISOString()]);
            console.log(currentRmInstances);
            if(currentRmInstances !== null && currentRmInstances.length !==0) { 
                return currentRmInstances[0];
            }
            else return [];
        } catch(error) {
            console.log("Error Occurred while getting latest RM Instance ---", error);
            return [];
        }
        
    }

    async cancelRmInstances(rmId) {
        // cancellation_date : new Date().toISOString(),
        // console.log("-----------Removing RM Instances ---------------");
        let cancelledRmInstances = await meetingModel.findAll({
            where : { rm_id : rmId, start_date : {
                [Op.gt] : new Date().toISOString()
            } }
        });
        
        let meetingIds = [];
        let stateMachineArn = [];
        if(cancelledRmInstances !== null || cancelledRmInstances['dataValues'] !== undefined) {
            meetingIds = cancelledRmInstances.map(x => x['dataValues']['id']);
            stateMachineArn = cancelledRmInstances.map(x => x['dataValues']['arnName']);
            console.log("arnNames are : ", stateMachineArn);
            // return meetingIds
        }
        if(meetingIds !== null && meetingIds.length !== 0) {
            await meetingInviteeModel.destroy({ where : {
                meeting_id : {
                    [Op.in] : meetingIds
                }
            }})
            console.log("------------Meetings ids to be deleted : ----------", meetingIds)
            await meetingModel.destroy({ 
                where : {
                    id : {
                        [Op.in] : meetingIds
                    }
                }
            });

            return stateMachineArn;
        }

       
    }

    async checkIfCertTrigForMeeting(meetingId) {
        try {
            console.log(`-----Getting today's meeting for meeting id : ${meetingId}`);
            let query = 'SELECT emailsent_date FROM meeting_invites LEFT JOIN  meetings on meetings.id = meeting_invites.meeting_id where 1= 1 and meetings.meeting_id = ?';
            let emailSentDatesForExistingMeeting = await pool.query(query, [meetingId]);
            console.log(emailSentDatesForExistingMeeting);
            let isSent = false;
            if(emailSentDatesForExistingMeeting !== null && emailSentDatesForExistingMeeting.length !==0) { 
                for(let i=0;i< emailSentDatesForExistingMeeting.length ; i ++) {
                    isSent = emailSentDatesForExistingMeeting[i].emailsent_date !== null;
                    if(isSent) {
                        break;
                    }
                }
                return isSent
            }
            else return isSent;
        } catch(error) {
            console.log("Error Occurred while getting dates for existing meeting ---", error);
           return false;
        }
    }

    async getMeetingByIcalUid(iCalUid) {
        try {
            let startDate = new Date().toISOString().slice(0,10);
            let endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate = endDate.toISOString().slice(0,10);

            console.log(`-----Getting today's meeting for iCalUid : ${iCalUid} with start : ${startDate} and end : ${endDate}`);

            let meetingIdQuery = `select id, meeting_id from meetings where 
            outlook_meeting_id='${iCalUid}' and start_date >= '${startDate} 00:00:00' and start_date <= '${endDate} 00:00:00'`;

            let meetingId = await pool.query(meetingIdQuery);
            // console.log("Meeting id is : " , meetingId);

            let query = `select * from meeting_invites where meeting_id in (${meetingId[0].id}) 
            and emailsent_date is not null`;
            
            let isCertTriggered = await pool.query(query);
            console.log("details are : " , isCertTriggered);

            if(isCertTriggered.length > 0) {
                return {isSent : true , meetingId : meetingId[0].meeting_id };
            } else return {isSent : false , meetingId : meetingId[0].meeting_id };
        } catch(error) {
            console.log('Error Occurred while getting existing meeting data for iCalUid : ' , iCalUid, " : ", error);
            return {isSent : false , meetingId : 0 };
        }
    }
}

module.exports = new MeetingRepository();