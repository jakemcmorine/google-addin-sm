const app = require('../index');
const fs = require('fs');
//const debug = require('debug')('server:server');
const https = require('https');

/** Get port from environment and store in Express. **/
const port = normalizePort(CONFIG.port || '5050');
app.set('port', port);

/** Create HTTP server. **/
// const server = http.createServer(app);
const server = https.createServer({
    key: fs.readFileSync('./server.key'),
    cert: fs.readFileSync('./server.cert')
}, app);


/** Listen on provided port, on all network interfaces. **/
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/** Normalize a port into a number, string, or false. **/

function normalizePort(arg) {
    var port = parseInt(arg, 10);
    if (isNaN(port)) {
        return arg; // named pipe
    }
    if (port >= 0) {
        return port;// port number
    }
    return false;
}

/** Event listener for HTTP server "error" event. **/
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default: throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    //debug('Listening on ' + bind);
    console.log('Environment:', CONFIG.app_env ? CONFIG.app_env : "local");
    console.log('Server listenning on port:', addr.port);
}
