const express = require('express');
const userRouter = express.Router();
const userController = require('../controllers/UserController')

userRouter.get('/userMail/:emailId', userController.userDetailsByEmailId);
userRouter.post('/activateApps', userController.checkAccess);
userRouter.post('/getUserData', userController.getUserData);

module.exports = userRouter;
