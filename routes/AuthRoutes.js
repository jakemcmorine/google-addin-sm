const express = require('express');
const authRouter = express.Router();

const authController = require('../controllers/AuthController');

authRouter.all('/auth-notification', authController.gAuthUser);
authRouter.all('/webhook-subscription', authController.webhookNotification);
authRouter.post('/refreshAccessToken', authController.refreshAccessTokenCall);
authRouter.post('/validateRefreshToken', authController.validateRefreshToken);

authRouter.all('/auth-token', (req, res) => {
    res.send("success");
});


module.exports = authRouter;