const express = require('express')
const meetingRouter = express.Router()
const meetinglists = require('../controllers/MeetingController')

meetingRouter.get('/:userId', meetinglists.meetingCountOfUser);

meetingRouter.post('/checkRecurringMeetings', meetinglists.cronCheckRecurringMeetings);

module.exports = meetingRouter;
