const dotenv = require('dotenv');//instatiate environment variables

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'dev') {
    dotenv.config({ path: '.env-development' });
} else if (process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'production') {
    dotenv.config({ path: '.env-production' });
} else {
    dotenv.config({ path: '.env-local' });
}

CONFIG = {};

CONFIG.port = process.env.PORT;
CONFIG.app_env = process.env.APP_ENV;

CONFIG.redis_host = process.env.REDIS_HOST;
CONFIG.redis_port = process.env.REDIS_PORT;

CONFIG.mysql_host = process.env.DB_HOST;
CONFIG.mysql_port = process.env.DB_PORT;
CONFIG.mysql_user = process.env.DB_USER;
CONFIG.mysql_password = process.env.DB_PASSWORD;
CONFIG.mysql_database = process.env.DB_NAME;


CONFIG.SQS_URL               = process.env.SQS_URL;
CONFIG.SQS_ARN               = process.env.SQS_ARN;
CONFIG.SQS_ACCESS_KEY_ID     = process.env.SQS_ACCESS_KEY_ID;
CONFIG.SQS_SECRET_ACCESS_KEY = process.env.SQS_SECRET_ACCESS_KEY;
CONFIG.SQS_REGION            = process.env.SQS_REGION;