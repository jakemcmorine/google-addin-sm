module.exports = {

    RECURRING_MEETING: "recurringMeeting",
    NORMAL_MEETING: "normalMeeting",
    CANCELLED: "cancelled",
    SYNC: "sync",
    GOOGLE_CALENDAR_URL: "https://www.googleapis.com/calendar/v3/calendars/primary",
    EVENTS_API: "https://www.googleapis.com/calendar/v3/calendars/primary/events?singleEvents=false",
    GET_LATEST_EVENT : "https://www.googleapis.com/calendar/v3/calendars/primary/events?&timeZone=UTC&singleEvents=false&syncToken=",
    GET_EVENTS: "https://www.googleapis.com/calendar/v3/calendars/primary/events/",
    GET_PAGINATING_EVENT: "https://www.googleapis.com/calendar/v3/calendars/primary/events?&timeZone=UTC&singleEvents=false&pageToken=",
    DAY_INDEX: {
        '1': 'first',
        '2': 'second',
        '3': 'third',
        '4': 'fourth',
        '-1': 'last'
    },
    WEEKDAYS: {
        'SU': "sunday",
        "MO": "monday",
        "TU": "tuesday",
        "WE": "wednesday",
        "TH": "thursday",
        "FR": "friday",
        "SA": "saturday"
    },
    FIRST: "first",
    WEEKLY: "weekly",
    DAILY: "daily",
    MONTHLY: "monthly",
    RELATIVE_MONTHLY: "relativeMonthly",
    ABSOLUTE_MONTHLY: "absoluteMonthly",
    CONFIRMED: "confirmed",
    YEARLY : "yearly",
    ABSOLUTE_YEARLY: "absoluteYearly",
    RELATIVE_YEARLY: "relativeYearly"
}