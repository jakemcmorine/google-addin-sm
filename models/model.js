const mysql     = require('mysql');
const util      = require('util');


/*-- MySQL Connection Async/Await --*/
var pool = mysql.createPool({
    host: CONFIG.mysql_host
    ,port:CONFIG.mysql_port
    ,user: CONFIG.mysql_user
    ,password: CONFIG.mysql_password
    ,database: CONFIG.mysql_database
})
pool.query = util.promisify(pool.query);
pool.getConnection = util.promisify(pool.getConnection);

(async() =>{
    try{
      await pool.query('SELECT NOW() AS "theTime"');
      
     
      console.log("Mysql Connected");
    }catch(err){
      console.log(err);
    }
  })();
module.exports.pool = pool;




