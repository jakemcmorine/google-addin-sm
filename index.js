require('./config/config');

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const userRouter = require('./routes/UserRoutes');
const authRouter = require('./routes/AuthRoutes')
const meetingsRouter = require('./routes/MeetingRoutes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/assets'));

app.use('/auth', authRouter);
app.use('/user', userRouter);
app.use('/meetings', meetingsRouter);

app.get('/', (req, res) => {
    res.send("hello................")
});

//dev- domain verification
app.get('/google0afadd15a1eb317b.html', (req, res) => {
    res.sendFile(__dirname + '/google0afadd15a1eb317b.html')
});

//live domain verification
app.get('/googleedd26d687500e0b8.html', (req, res) => {
    res.sendFile(__dirname + '/googleedd26d687500e0b8.html')
});

module.exports = app;